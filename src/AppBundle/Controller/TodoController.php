<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Todo;
use AppBundle\Entity\User;
use AppBundle\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\CommentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use FOS\UserBundle\Util\LegacyFormHelper;
class TodoController extends Controller
{
    /**
     * @Route("/", name="todo_list")
     */
    public function listAction()
    {
       $todos = $this->getDoctrine()
       ->getRepository('AppBundle:Todo')
       ->findAll();

        return $this->render('todo/index.html.twig', array(
            'todos' => $todos
        ));       
    }
    /**
     * @Route("/todo/create", name="todo_create")
     */
    public function createAction(Request $request)
    {

        $todo = new Todo;

        $users = $this->getDoctrine()
        ->getRepository('AppBundle:User')
        ->findAll();

        $temp_users = array();
        foreach($users as $user) {
        $temp_users[$user->getUsername()] = $user->getId();
        }

           
        

        $form = $this->createFormBuilder($todo)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('wykona', ChoiceType::class, array('choices' => $temp_users, 'multiple' => true, 'expanded' => true, 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('category', ChoiceType::class, array('choices' => array('Praca' => 'Praca', 'Dom' => 'Dom', 'Inne' => 'Inne'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('due_date', DateTimeType::class, array('attr' => array('class' => 'formcontrol', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Create Todo', 'attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $form->handleRequest($request);

        

        

        if($form->isSubmitted() && $form->isValid()){
            //Get Data 
            $name = $form['name']->GetData();
            $wykona = $form['wykona']->GetData();
            $description = $form['description']->GetData();
            $category = $form['category']->GetData();
            $due_date = $form['due_date']->GetData();


       

            $now = new\DateTime('now');

            $todo->SetName($name);
           
            $todo->SetWykona($wykona);
            $todo->SetDescription($description);
            $todo->SetCategory($category);
            $todo->SetDueDate($due_date);
            $todo->SetCreateDate($now);

            $em = $this->getDoctrine()->getManager();

            $em->persist($todo);
            $em->flush();

            $this->addFlash(
                'notice',
                'Todo Added'
            );


            return $this->redirectToRoute('todo_list');

        }
        
        
        return $this->render('todo/create.html.twig', array(
            'form' => $form->createView()
        ));       
    }
    /**
     * @Route("/todo/edit/{id}", name="todo_edit")
     */
    public function editAction($id, Request $request){
         $todo = $this->getDoctrine()
       ->getRepository('AppBundle:Todo')
       ->find($id);


       
        $users = $this->getDoctrine()
        ->getRepository('AppBundle:User')
        ->findAll();

        $temp_users = array();
        foreach($users as $user) {
        $temp_users[$user->getUsername()] = $user->getId();
        }
       
            $now = new\DateTime('now');

            $todo->SetName($todo->getName());
            $todo->SetWykona($todo->getWykona());
            $todo->SetDescription($todo->getDescription());
            $todo->SetCategory($todo->getCategory());
            $todo->SetDueDate($todo->getDueDate());
            $todo->SetCreateDate($now);

        $form = $this->createFormBuilder($todo)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('wykona', ChoiceType::class, array('choices' => $temp_users, 'multiple' => true, 'expanded' => true, 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('category', ChoiceType::class, array('choices' => array('Praca' => 'Praca', 'Dom' => 'Dom', 'Inne' => 'Inne'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('due_date', DateTimeType::class, array('attr' => array('class' => 'formcontrol', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Update Todo', 'attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //Get Data 
            $name = $form['name']->GetData();
            $wykona = $form['wykona']->GetData();
            $description = $form['description']->GetData();
            $category = $form['category']->GetData();
            $due_date = $form['due_date']->GetData();


            $em = $this->getDoctrine()->getManager();
            $todo = $em->getRepository('AppBundle:Todo')->find($id);

          
            $em->flush();

            $this->addFlash(
                'notice',
                'Todo Updated'
            );

            return $this->redirectToRoute('todo_list');

        }

        return $this->render('todo/edit.html.twig', array(
            'todo' => $todo,
            'form' => $form->createView()
        ));        
    }
    /**
     * @Route("/todo/details/{id}", name="todo_details")
     */
    public function detailsAction(Request $request, $id){
       $todo = $this->getDoctrine()
       ->getRepository('AppBundle:Todo')
       ->find($id);
       
       $users = $this->getDoctrine()
       ->getRepository('AppBundle:User')
       ->findAll();

       $temp_users = array();
       foreach($users as $user) {
           $temp_users[$user->getId()] = $user->getUsername();
       }
       
        
        //Komentarze

        $comment = new Comment;
        $comment->setTodo($todo);
        $form = $this->createForm(CommentType::class);

        $form->handleRequest($request);

       if($form->isSubmitted() && $form->isValid()){

            $comment = $form->getData();
            $comment->setName($this->getUser()->getUsername());
            $comment->setAuthorEmail($this->getUser()->getEmail());
            $comment->setTodo($todo);

             $em = $this->getDoctrine()->getManager();
             $em->persist($comment);
             $em->flush();

             $this->addFlash(
                'succes',
                'Comment was send succesfully');

            return $this->redirectToRoute('todo_details', array(
                'id' => $todo -> getId()));
       
       
       }

        return $this->render('todo/details.html.twig', array(
            'todo' => $todo,
            'users' => $temp_users,
            'form' => $form->createView()
        ));       
    }
     /**
     * @Route("/todo/delete/{id}", name="todo_delete")
     */
    public function deleteAction($id){
            $em = $this->getDoctrine()->getManager();
            $todo = $em->getRepository('AppBundle:Todo')->find($id);

            $em->remove($todo);
            $em->flush();

            $this->addFlash(
                'notice',
                'Todo deleted'
            );

        return $this->redirectToRoute('todo_list');

    }

     /**
     * @Route("/register", name="user_add")
     */
    public function addUserAction(Request $request)
    {
       $user = new User;
       $form = $this->createFormBuilder($user)
       ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'Email', 'translation_domain' => 'FOSUserBundle'))
       ->add('username', TextType::class)
       ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Confirm Password'),
                'invalid_message' => 'Confirm Password',
            ))
        
       ->add('save', SubmitType::class, array('label' => 'Add', 'attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
        ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //Get Data 
            $username = $form['username']->GetData();
            $user->SetUsername($username);

            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'notice',
                'User Added'
            );

            return $this->redirectToRoute('todo_list');
        }

    return $this->render('todo/register.html.twig', array(
            'form' => $form->createView()
        )); 
           
    
}

}
